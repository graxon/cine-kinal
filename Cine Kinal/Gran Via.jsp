<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <title>Cine Kinal</title>

  <link rel='stylesheet prefetch' href='http://tympanus.net/Development/Slicebox/css/demo.css'>

    <link rel="stylesheet" href="css/style.css" media="screen" type="text/css" />

    <script src="js/modernizr.js"></script>
</head>
<body style="color:#424242">

  <h1 style="color:#424242">Caretelera</h1>
  <h2 style="color:#424242">Gran Via</h2>

  <div class="container">
  <div class="wrapper">

    <ul id="sb-slider" class="sb-slider">
      <li>
        <a href="../images/i.jpg" target="_blank"><img src="images/1.jpg" alt="image1"/></a>
        <input style="color:#424242; font-size:15px" type="button" value="Haz click aqui para comprar boletos"  onclick="window.location='1.html';" />
        <div>
        <h3 style="color:white; font-size:25px">Tres Metros Sobre el Cielo</h3>
      </div>
      </li>
      <li>
        <a href="../images/2.jpg" target="_blank"><img src="images/2.jpg" alt="image2"/></a>
        <input style="color:#424242; font-size:15px" type="button" value="Haz click aqui para comprar boletos"  onclick="window.location='2.html';" />
        <h3 style="color:white; font-size:25px">Crepusculo Amanecer</h3>
      </li>
      <li>
        <a href="../images/3.jpg" target="_blank"><img src="images/3.jpg" alt="image1"/></a>
        <input style="color:#424242; font-size:15px" type="button" value="Haz click aqui para comprar boletos"  onclick="window.location='3.html';" />
        <h3 style="color:white; font-size:25px">Pinguinos de Madagascar</h3>
      </li>
      <li>
        <a href="images/4.jpg" target="_blank"><img src="images/4.jpg" alt="image1"/></a>
        <input style="color:#424242; font-size:15px" type="button" value="Haz click aqui para comprar boletos"  onclick="window.location='4.html';" />
        <h3 style="color:white; font-size:25px">Tres Dias Para Matar</h3>
      </li>
      <li>
        <a href="images/5.jpg" target="_blank"><img src="images/5.jpg" alt="image1"/></a>
        <input style="color:#424242; font-size:15px" type="button" value="Haz click aqui para comprar boletos"  onclick="window.location='5.html';" />
        <h3 style="color:white; font-size:25px">Ajestes de Desorden</h3>
      </li>
      <li>
        <a href="images/6.jpg" target="_blank"><img src="images/6.jpg" alt="image1"/></a>
        <input style="color:#424242; font-size:15px" type="button" value="Haz click aqui para comprar boletos"  onclick="window.location='6.html';" />
        <h3 style="color:white; font-size:25px">La Mejor Oferta</h3>
      </li>s
      <li>
        <a href="images/7.jpg" target="_blank"><img src="images/7.jpg" alt="image1"/></a>
        <input style="color:#424242; font-size:15px" type="button" value="Haz click aqui para comprar boletos"  onclick="window.location='7.html';" />
        <h3 style="color:white; font-size:25px">Bajo la Misma Estrella</h3>
      </li>
      <li>
        <a href="images/8.jpg" target="_blank"><img src="images/8.jpg" alt="image1"/></a>
        <input style="color:#424242; font-size:15px" type="button" value="Haz click aqui para comprar boletos"  onclick="window.location='8.html';" />
        <h3 style="color:white; font-size:25px">Busqueda Implacable 3</h3>
      </li>
      <li>
        <a href="images/9.jpg" target="_blank"><img src="images/9.jpg" alt="image1"/></a>
        <input style="color:#424242; font-size:15px" type="button" value="Haz click aqui para comprar boletos"  onclick="window.location='9.html';" />
        <h3 style="color:white; font-size:25px">Dos Tontos Todavia mas Tontos</h3>
      </li>
      <li>
        <a href="images/10.jpg" target="_blank"><img src="images/10.jpg" alt="image1"/></a>
        <input style="color:#424242; font-size:15px" type="button" value="Haz click aqui para comprar boletos"  onclick="window.location='10.html';" />
        <h3 style="color:white; font-size:25px">La Batalla de los 5 Ejercitos</h3>
      </li>
    </ul>

    <div id="shadow" class="shadow"></div>

    <div id="nav-arrows" class="nav-arrows">
      <a href="#">Next</a>
      <a href="#">Previous</a>
    </div>

  </div>
</div>

  <script src='http://codepen.io/assets/libs/fullpage/jquery.js'></script>
  <script src='http://tympanus.net/Development/Slicebox/js/jquery.slicebox.js'></script>

  <script src="js/index.js"></script>

</body>
</html>