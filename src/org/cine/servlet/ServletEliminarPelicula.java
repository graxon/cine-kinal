package org.cine.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.cine.Manejadores.ManejadorPelicula;
import org.cine.bean.Pelicula;
import org.cine.db.Conexion;

public class ServletEliminarPelicula extends HttpServlet{
	public void doGet(HttpServletRequest peticion,HttpServletResponse respuesta)throws IOException,ServletException{
		doPost(peticion,respuesta);
	}
	public void doPost(HttpServletRequest peticion,HttpServletResponse respuesta)throws IOException,ServletException{
		RequestDispatcher despachador=null;
		
		ManejadorPelicula.INSTANCIA.eliminar(Integer.parseInt(peticion.getParameter("idPelicula")));
		
		despachador=peticion.getRequestDispatcher("ServletListarPelicula.do");
		despachador.forward(peticion, respuesta);
	}
}
