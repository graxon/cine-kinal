package org.cine.servlet;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

import org.cine.bean.Pelicula;
import org.cine.Manejadores.ManejadorPelicula;

public class ServletAgregarPelicula extends HttpServlet {
	public void doGet(HttpServletRequest peticion,HttpServletResponse respuesta)throws IOException,ServletException{
		doPost(peticion,respuesta);
	}
	public void doPost(HttpServletRequest peticion,HttpServletResponse respuesta)throws IOException,ServletException{
		RequestDispatcher despachador=null;
		Pelicula c=new Pelicula(
				0,
				peticion.getParameter("txtnombrePelicula"),
				peticion.getParameter("txtdescripcionPelicula"),
				peticion.getParameter("txttrailerPelicula"),
				Integer.parseInt(peticion.getParameter("txtduracionPelicula")));
				
		
		ManejadorPelicula.INSTANCIA.agregar(c);
		despachador=peticion.getRequestDispatcher("ServletListarPelicula.do");
		despachador.forward(peticion, respuesta);
	}
}

