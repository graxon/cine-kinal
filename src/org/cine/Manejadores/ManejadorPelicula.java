package org.cine.Manejadores;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.cine.bean.Pelicula;
import org.cine.db.Conexion;

public class ManejadorPelicula {
	public static final ManejadorPelicula INSTANCIA=new ManejadorPelicula();
	public ArrayList<Pelicula> listar(){
		ResultSet rs=Conexion.getInstancia().obtenerConsulta("SELECT * from Pelicula" );
		ArrayList<Pelicula>  lista=new ArrayList<Pelicula> ();
		try {
			while (rs.next()) {
				lista.add(new Pelicula(rs.getInt("idPelicula"),rs.getString("nombrePelicula"),rs.getString("descripcionPelicula"),rs.getString("trailerPelicula"),rs.getInt("duracionPelicula")));
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lista;
	}
	public void agregar(Pelicula c){
		Conexion.getInstancia().ejecutarConsulta("insert into pelicula values("+c.getIdPelicula()+","
				+ "'"+c.getNombrePelicula()+"','"+c.getDescripcionPelicula()+"','"+c.getTrailerPelicula()+"',"+c.getDuracionPelicula()+")");
	}
	public Pelicula buscar(Integer id){
		Pelicula c=new Pelicula();
		c.setIdPelicula(-1);
		ResultSet rs=Conexion.getInstancia().obtenerConsulta("select * from Pelicula where idPelicula="+id);
		
		try {
			while (rs.next()) {
				c.setIdPelicula(rs.getInt("idPelicula"));
				c.setNombrePelicula(rs.getString("nombrePelicula"));
				c.setDescripcionPelicula(rs.getString("descripcionPelicula"));
				c.setTrailerPelicula(rs.getString("trailerPelicula"));
				c.setDuracionPelicula(rs.getInt("duracionPelicula"));
				
			}
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return c;
	}
	public void editar(Pelicula c){
		
		Conexion.getInstancia().ejecutarConsulta("update pelicula set nombrePelicula='"+c.getNombrePelicula()+"',descripcionPelicula='"+c.getDescripcionPelicula()+"',trailerPelicula='"+c.getTrailerPelicula()+"',duracionPelicula="+c.getDuracionPelicula());
	}
	
	public void eliminar(Integer id){
		Conexion.getInstancia().ejecutarConsulta("delete from Pelicula where idPelicula="+id);
	}
}
