<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cine</title>
</head>
<body>
<h2 class="sub-header">LISTADO DE PELICULAS</h2>
				<div class="table-responsive">
					<table class="table table-striped">
						<thead>
							<th>----- Nombre---- </th>
							<th>----- descripcion---- </th>
							<th>---- trailer---- </th>
							<th>---- Duracion---- </th>
							

						</thead>
						<tbody>
							<c:forEach var="pelicula" items="${listaPeliculas}">
								<tr>
									<td>${pelicula.getNombrePelicula()}</td>
									<td>${pelicula.getDescripcionPelicula()}</td>
									<td>${pelicula.getTrailerPelicula()}</td>
									<td>${pelicula.getDuracionPelicula()}</td>
									
									<td>
									 <a
										href="ServletEliminarPelicula.do?idPelicula=${pelicula.getIdPelicula()}">ELIMINAR</a>
									</td>
									
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				</div>
				<hr>
		
      
	<form id="form" action="ServletAgregarPelicula.do" method="GET">
	<fieldset>
	<label><strong>Nombre :</strong>
		<input type="text" name="txtnombrePelicula"   Style="background-color:white"  >
		</label>
		<label><strong>descripcion:</strong>
	 <input type="text" Style="background-color:white" name="txtdescripcionPelicula">
	 </label>
	 <label><strong>Trailer:</strong>
		 <input type="text" Style="background-color:white" name="txttrailerPelicula">
		 </label>
		 <label><strong>Duracion:</strong>
		 <input type="text" Style="background-color:white" name="txtduracionPelicula">
		 </label>
		 
		<input class="button" type="submit" value="Nuevo" >
		
	</fieldset>
	</form>

</body>
</html>